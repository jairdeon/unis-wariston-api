<?php

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(App\Models\Student::class, function (Faker $faker) {
    $courses = Course::all();

    return [
        'name' => $faker->name,
        'course_id' => $courses->random()->id
    ];
});
