<?php

use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    public function run()
    {
        $courses = [
            'Ciência da Computação',
            'Sistemas da Informação',
            'Análise e desenvolvimento de sistemas'
        ];

        foreach($courses as $course) factory(\App\Models\Course::class, 1)->create(['name' => $course]);
    }
}
