<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\CoursesResource;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index() {
        return CoursesResource::collection(Course::all());
    }

    public function show(Course $course) {
        return new CoursesResource($course);
    }

    public function store(Request $request) {
        $course = Course::create($request->all());
        return new CoursesResource($course);
    }

    public function update(Request $request, Course $course) {
        $course->fill($request->all());
        $course->save();
        return new CoursesResource($course);
    }

    public function destroy(Course $course) {
        Course::destroy($course->id);
    }
}
