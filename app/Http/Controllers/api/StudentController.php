<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\StudentsResource;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index() {
        return StudentsResource::collection(Student::all());
    }

    public function show(Student $student) {
        return new StudentsResource($student);
    }

    public function store(Request $request) {
        $student = Student::create($request->all());
        return new StudentsResource($student);
    }

    public function update(Request $request, Student $student) {
        $student->fill($request->all());
        $student->save();
        return new StudentsResource($student);
    }

    public function destroy(Student $student) {
        Student::destroy($student->id);
    }

    public function search($param) {
        $query = Student::where('name', 'like', '%' . $param . '%');
        return StudentsResource::collection($query->get());
    }
}
