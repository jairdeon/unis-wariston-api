<?php

namespace App\Http\Resources\Api;

use App\Http\Resources\Api\CoursesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StudentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'course_id' => $this->course_id,
            'course' => new CoursesResource($this->course)
        ];
    }
}
