<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
  protected $fillable = ['name', 'course_id'];
  use Sluggable;
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  public function course() {
    return $this->belongsTo(Course::class);
  }
}
