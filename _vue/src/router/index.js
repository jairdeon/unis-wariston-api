import CoursesIndex from '@/components/Courses/Index'
import CoursesCreate from '@/components/Courses/Create'
import CoursesShow from '@/components/Courses/Show'

import StudentsIndex from '@/components/Students/Index'
import StudentsCreate from '@/components/Students/Create'
import StudentsShow from '@/components/Students/Show'

const routes = [
  {path: '/', name: 'HelloWorld', component: StudentsIndex},
  {path: '/courses', name: 'CoursesIndex', component: CoursesIndex},
  {path: '/courses/create', name: 'CoursesCreate', component: CoursesCreate},
  {path: '/courses/:id', name: 'CoursesShow', component: CoursesShow},
  {path: '/students', name: 'StudentsIndex', component: StudentsIndex},
  {path: '/students/create', name: 'StudentsCreate', component: StudentsCreate},
  {path: '/students/:id', name: 'StudentsShow', component: StudentsShow}
]

export default routes
