import Vue from 'vue'

export default {
  state: {
    courseIndex: [],
    courseSearch: [],
    courseShow: {}
  },
  mutations: {
    updateCourseIndex (state, data) {
      state.courseIndex = data
    },
    updateCourseShow (state, data) {
      state.courseShow = data
    },
    updateCoursesSearch (state, data) {
      state.courseSearch = data
    }
  },
  actions: {
    getCourses (context) {
      Vue.http.get('courses').then(response => {
        context.commit('updateCourseIndex', response.data)
      })
    },
    getCoursesSearch (context, param) {
      let condition = param && param.length ? param.length : 0
      if (condition <= 0) {
        Vue.http.get('courses').then(response => {
          context.commit('updateCoursesSearch', response.data)
        })
      } else {
        Vue.http.get('courses/search/' + param).then(response => {
          context.commit('updateCoursesSearch', response.data)
        })
      }
    },
    getCourse (context, id) {
      Vue.http.get('courses/' + id).then(response => {
        context.commit('updateCourseShow', response.data.data)
      })
    },
    newCourse (context, data) {
      Vue.http.post('courses', data)
    },
    updateCourse (context, params) {
      Vue.http.put('courses/' + params.id, params)
    },
    deleteCourse (context, id) {
      Vue.http.delete('courses/' + id).then(() => {
        return {message: 'Sucesso'}
      }, () => {
        alert('Não é possível remover este curso pois um ou mais alunos pertencem a ele.')
      })
    }
  }
}
