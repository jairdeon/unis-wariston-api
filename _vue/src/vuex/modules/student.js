import Vue from 'vue'

export default {
  state: {
    studentIndex: [],
    studentSearch: [],
    studentShow: {}
  },
  mutations: {
    updateStudentIndex (state, data) {
      state.studentIndex = data
    },
    updateStudentShow (state, data) {
      state.studentShow = data
    },
    updateStudentsSearch (state, data) {
      state.studentSearch = data
    }
  },
  actions: {
    getStudents (context) {
      Vue.http.get('students').then(response => {
        context.commit('updateStudentIndex', response.data)
      })
    },
    getStudentsSearch (context, param) {
      let condition = param && param.length ? param.length : 0
      if (condition <= 0) {
        Vue.http.get('students').then(response => {
          context.commit('updateStudentsSearch', response.data)
        })
      } else {
        Vue.http.get('students/search/' + param).then(response => {
          context.commit('updateStudentsSearch', response.data)
        })
      }
    },
    getStudent (context, id) {
      Vue.http.get('students/' + id).then(response => {
        context.commit('updateStudentShow', response.data.data)
      })
    },
    newStudent (context, data) {
      Vue.http.post('students', data)
    },
    updateStudent (context, params) {
      Vue.http.put('students/' + params.id, params)
    },
    deleteStudent (context, id) {
      Vue.http.delete('students/' + id)
    }
  }
}
