import course from './modules/course'
import student from './modules/student'

export default {
  modules: {
    course: course,
    student: student
  }
}
