<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1">
    <title>Unis - API</title>
    <link href="{{ url('static/css/app.7aa421780d1896369b03fbcd982443bf.css') }}" rel=stylesheet>
</head>
<body>
<div id="app"></div>
<script type=text/javascript src="{{ url('static/js/manifest.2ae2e69a05c33dfc65f8.js') }}"></script>
<script type=text/javascript src="{{ url('static/js/vendor.f10a6e2f929a8d2087fb.js') }}"></script>
<script type=text/javascript src="{{ url('static/js/app.8ade90830e4dad6a4d16.js') }}"></script>
</body>
</html>
