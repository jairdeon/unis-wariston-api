####Aplicação desenvolvida por Jair Deon e Rodrigo Lopes para o curso Design e Desenvolvimento de Banco de Dados.

Proposta: Desenvolver uma aplicação com conceito RESTFUL com os principios básicos dos verbos http:</p>
- GET
- POST
- PUT
- DELETE

Sistema:
O sistema desenvolvido é um cadastro básico de alunos e cursos. Os alunos sempre pertencem a um curso.

##**Tecnologias utilizadas**
- Laravel 5.6
- Vue 2

##Ambiente deploy:
- PHP 7.2
- Mysql

###Demo: [http://68.183.31.5]
**API: Cursos**
```
GET: /api/courses

{
  "data": [
    {
      "id": 1,
      "name": "Ciência da computação",
      "slug": "ciencia-da-computacao"
    },
    {
      "id": 2,
      "name": "Sistemas da informação",
      "slug": "sistemas-da-informacao"
    },
    {
      "id": 3,
      "name": "Análise e desenvolvimento de sistemas",
      "slug": "analise-e-desenvolvimento-de-sistemas"
    }
  ]
}
```


```
POST: /api/courses
Obrigatório: [name]
```

```
SHOW: /api/courses/{id}
Obrigatório: [id] ou [slug]

Retorno:
{
  "data": {
    "id": 1,
    "name": "Ciência da computação",
    "slug": "ciencia-da-computacao"
  }
}
```

```
PUT: /api/courses/{id}
Obrigatório: [id] [name]
```

```
DELETE: /api/courses/{id}
Obrigatório: [id]
```


**API: Alunos**
```
GET: /api/students

{
  "data": [
    {
      "id": 1,
      "name": "Jair Deon",
      "slug": "jair-deon",
      "course_id": 1,
      "course": {
        "id": 1,
        "name": "Ciência da computação",
        "slug": "ciencia-da-computacao"
      }
    },
    {
      "id": 2,
      "name": "Rodrigo Lopes",
      "slug": "rodrigo-lopes",
      "course_id": 2,
      "course": {
        "id": 2,
        "name": "Sistemas da informação",
        "slug": "sistemas-da-informacao"
      }
    }
  ]
}
```


```
POST: /api/students
Obrigatório: [name] [course_id]
```

```
SHOW: /api/students/{id}
Obrigatório: [id] ou [slug]

Retorno:
{
  "data": {
    "id": 1,
    "name": "Jair Deon",
    "slug": "jair-deon",
    "course_id": 1,
    "course": {
      "id": 1,
      "name": "Ciência da computação",
      "slug": "ciencia-da-computacao"
    }
  }
}
```

```
PUT: /api/students/{id}
Obrigatório: [id] [name]
```

```
DELETE: /api/students/{id}
Obrigatório: [id]
```

